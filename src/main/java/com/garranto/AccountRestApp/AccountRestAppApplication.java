package com.garranto.AccountRestApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class AccountRestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountRestAppApplication.class, args);
	}

}
