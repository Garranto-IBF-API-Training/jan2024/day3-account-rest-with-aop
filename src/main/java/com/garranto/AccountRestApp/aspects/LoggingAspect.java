package com.garranto.AccountRestApp.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect {


    private Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Before("execution(* com.garranto.*.*.*.*Handler(..))")
    public void beforeAdvice(JoinPoint joinpoint) {
        logger.info(
                joinpoint.getSignature().getName()
                        +" Method started its execution with "
                        + Arrays.toString(joinpoint.getArgs()));
    }
}
//aspectj