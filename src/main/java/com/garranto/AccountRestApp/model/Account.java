package com.garranto.AccountRestApp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Account {

    @Id
    private String accountNumber;

    @Column(name = "accountType",nullable = false)
    private String accType;

    private double balance;

    private  String currency;

    private String accHolderName;

    public Account(String accountNumber, String accType, double balance, String currency, String accHolderName) {
        this.accountNumber = accountNumber;
        this.accType = accType;
        this.balance = balance;
        this.currency = currency;
        this.accHolderName = accHolderName;
    }

    public Account(){}

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccHolderName() {
        return accHolderName;
    }

    public void setAccHolderName(String accHolderName) {
        this.accHolderName = accHolderName;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber='" + accountNumber + '\'' +
                ", accType='" + accType + '\'' +
                ", balance=" + balance +
                ", currency='" + currency + '\'' +
                ", accHolderName='" + accHolderName + '\'' +
                '}';
    }
}
