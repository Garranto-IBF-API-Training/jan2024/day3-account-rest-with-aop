package com.garranto.AccountRestApp.controller;

import com.garranto.AccountRestApp.dto.AccountBalanceDTO;
import com.garranto.AccountRestApp.dto.AccountListDTO;
import com.garranto.AccountRestApp.exceptions.AccountAlreadyPresentException;
import com.garranto.AccountRestApp.exceptions.AccountNotPresentException;
import com.garranto.AccountRestApp.model.Account;
import com.garranto.AccountRestApp.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class AccountController {


    @Autowired
    private AccountService accountService;

    @GetMapping(value = "/test")
    public String testHandler(){
        return "Weclome to my Account REST API";
    }


//    get all accounts
//    resource representation, response headers and response status
//    ResponseEntity
    @GetMapping(value = "/accounts")
    public ResponseEntity<AccountListDTO> getAllAccountsHandler(){

//        read all the accounts from the account table
//        apply business logic on the data read from the table
//        create response entity with the processed data
//        return data
        ResponseEntity<AccountListDTO> responseEntity;
        AccountListDTO accountList =  accountService.getAllAccounts();
        responseEntity = new ResponseEntity<>(accountList, HttpStatus.OK);

        return responseEntity;


    }

    @PostMapping(value = "/accounts")
    public ResponseEntity<String> addNewAccount(@RequestBody Account account){
//        get the account from the request body
//        add the account to the database using the service
//        service will add the account if not exists
//        else throw the exception

        ResponseEntity<String> responseEntity;

        try {
            accountService.addNewAccount(account);
            responseEntity = new ResponseEntity<>("Account added successfully",HttpStatus.CREATED);
        } catch (AccountAlreadyPresentException e) {
            responseEntity = new ResponseEntity<>("Account creation Failed. Acount with the same number already present", HttpStatus.CONFLICT);
        }

        return responseEntity;
    }


    @GetMapping(value = "/accounts/{accNumber}")
    public ResponseEntity<?> getAccountByAccountNumberHandler(@PathVariable("accNumber") String accountNumber){
        ResponseEntity<?> responseEntity;

        try {
            Account account = accountService.getAccountByAccountNumber(accountNumber);
            responseEntity = new ResponseEntity<Account>(account,HttpStatus.OK);
        } catch (AccountNotPresentException e) {
            responseEntity = new ResponseEntity<String>("Failed to get the account. Account does not exists", HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }

    @GetMapping(value = "/accounts/{accNumber}/balance")
    public ResponseEntity<?> getBalanceHandler(@PathVariable("accNumber") String accountNumber){
        ResponseEntity<?> responseEntity;

        try {
            AccountBalanceDTO balance = accountService.getAccountBalance(accountNumber);
            responseEntity = new ResponseEntity<AccountBalanceDTO>(balance,HttpStatus.OK);
        } catch (AccountNotPresentException e) {
            responseEntity = new ResponseEntity<String>("Account does not exists", HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }
}


//Request----->controller(has to take action)----->service-----Repository

//service will contain actions(core functionalities)

//how service will access data ----repository

//update --- put

//delete --- Delete