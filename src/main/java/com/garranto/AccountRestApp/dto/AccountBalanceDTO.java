package com.garranto.AccountRestApp.dto;

public class AccountBalanceDTO {

    private double balance;
    private String currency;

    public AccountBalanceDTO(double balance, String currency) {
        this.balance = balance;
        this.currency = currency;
    }
    public double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
