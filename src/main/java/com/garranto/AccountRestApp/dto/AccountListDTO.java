package com.garranto.AccountRestApp.dto;

import com.garranto.AccountRestApp.model.Account;

import java.util.List;

public class AccountListDTO {

    private int records;
    private List<Account> accounts;

    public AccountListDTO(int records, List<Account> accounts) {
        this.records = records;
        this.accounts = accounts;
    }

    public int getRecords() {
        return records;
    }

    public List<Account> getAccounts() {
        return accounts;
    }
}
