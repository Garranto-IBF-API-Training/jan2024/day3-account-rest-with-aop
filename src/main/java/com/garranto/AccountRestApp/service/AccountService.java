package com.garranto.AccountRestApp.service;

import com.garranto.AccountRestApp.dto.AccountBalanceDTO;
import com.garranto.AccountRestApp.dto.AccountListDTO;
import com.garranto.AccountRestApp.exceptions.AccountAlreadyPresentException;
import com.garranto.AccountRestApp.exceptions.AccountNotPresentException;
import com.garranto.AccountRestApp.model.Account;
import com.garranto.AccountRestApp.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
//    implement all the core logic operates on account data

    @Autowired
    private AccountRepository accountRepository;
    public AccountListDTO getAllAccounts(){

        List<Account> accountList = accountRepository.findAll();
        return new AccountListDTO(accountList.size(),accountList);
    }

    public boolean addNewAccount(Account account) throws AccountAlreadyPresentException{

        Optional<Account> optionalAccount = accountRepository.findById(account.getAccountNumber());
        if(optionalAccount.isEmpty()){

            accountRepository.save(account);
            return true;
        }

        throw new AccountAlreadyPresentException();
    }


    public Account getAccountByAccountNumber(String accountNumber) throws AccountNotPresentException{

        Optional<Account> optionalAccount = accountRepository.findById(accountNumber);

        if(optionalAccount.isPresent()){
            Account account = optionalAccount.get();
            return account;
        }

        throw new AccountNotPresentException();
    }

    public AccountBalanceDTO getAccountBalance(String accountNumber) throws AccountNotPresentException{
        Optional<Account> optionalAccount = accountRepository.findById(accountNumber);

        if(optionalAccount.isPresent()){
            Account account = optionalAccount.get();
            AccountBalanceDTO balanceDTO = new AccountBalanceDTO(account.getBalance(),account.getCurrency());
            return balanceDTO;
        }

        throw new AccountNotPresentException();
    }
}




//begin()
//operation 1
//operation 2
//operation 3
//commit()





